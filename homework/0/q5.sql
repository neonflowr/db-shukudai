-- !!

with cbm(name, date_month, n) as (
select artist.name, release_info.date_month, COUNT() as n from release 
inner join release_info on release.id = release_info.release
inner join artist on artist.id = release.artist_credit
where release.artist_credit in (
select artist.id from artist
where artist.type = 1 and artist.name like 'Elvis%'
) group by artist.name, release_info.date_month having release_info.date_month > 0
), maxm(name, n) as (
select cbm.name, MAX(cbm.n) as mx from cbm
group by cbm.name
)
select cbm.name, MIN(cbm.date_month), cbm.n from cbm 
inner join maxm on maxm.name = cbm.name 
where cbm.n = maxm.n
group by cbm.name, cbm.n
order by cbm.n desc, cbm.name asc;
