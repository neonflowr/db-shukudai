-- duckdb
select work_type.name, a.name, b.mx, a.comment from work a
inner join 
(
select type, MAX(LEN(comment)) as mx
from work
group by type
having MAX(LEN(comment)) > 0
) b on a.type = b.type and LEN(a.comment) = b.mx
inner join work_type on a.type = work_type.id
order by a.type asc, a.name asc;

-- sqlite
select work_type.name, a.name, b.mx, a.comment from work a
inner join 
(
select type, MAX(LENGTH(comment)) as mx
from work
group by type
having MAX(LENGTH(comment)) > 0
) b on a.type = b.type and LENGTH(a.comment) = b.mx
inner join work_type on a.type = work_type.id
order by a.type asc, a.name asc;
