-- Groups formed in the US by decade

-- duckdb
with groupdec(name, begin_date_year, decade) as (
    select name, begin_date_year, CAST((FLOOR(begin_date_year / 10) * 10) AS INT) as decade from artist
    where artist.area in (222, 223) and artist.type = 2 and begin_date_year >= 1900 and begin_date_year <= 2023
    order by begin_date_year asc
)
select CONCAT(CAST(decade AS varchar), 's') as dtext, COUNT() 
from groupdec
group by decade
order by decade;

-- sqlite
with groupdec(name, begin_date_year, decade) as (
    select name, begin_date_year, CAST((FLOOR(begin_date_year / 10) * 10) AS INT) as decade from artist
    where artist.area in (222, 223) and artist.type = 2 and begin_date_year >= 1900 and begin_date_year <= 2023
    order by begin_date_year asc
)
select (CAST(decade AS varchar) || 's') as dtext, COUNT() 
from groupdec
group by decade
order by decade;
