-- sqlite
with aliases(id, name, alias_name) as (
	select artist.id, artist.name, artist_alias.name as alias_name from artist
	inner join artist_alias on artist_alias.artist = artist.id
	where artist.name like '% john'
), ca(id, name, alias_name) as (
	select a.id, a.name, a.alias_name from aliases a 
	where a.alias_name != '' and a.alias_name not like '%john%'
    order by a.name asc, a.alias_name asc
)
select b.name, COUNT(), group_concat(b.alias_name) from ca b
group by b.name
order by b.name; 

-- duckdb
with aliases(id, name, alias_name) as (
	select artist.id, artist.name, artist_alias.name as alias_name from artist
	inner join artist_alias on artist_alias.artist = artist.id
	where artist.name like '% John'
), ca(id, name, alias_name) as (
	select a.id, a.name, a.alias_name from aliases a 
	where a.alias_name != '' and a.alias_name not ilike '%john%'
)
select b.name, COUNT(), string_agg(b.alias_name, ',' order by b.alias_name asc) from ca b
group by b.name
order by b.name; 
