select release.name, artist_credit.name as artist, release_info.date_day, release_info.date_month, release_info.date_year, medium_format.name as medium from release
inner join release_info on release_info.release = release.id
inner join artist_credit on release.artist_credit = artist_credit.id
inner join medium on release.id = medium.release
inner join medium_format on medium.format = medium_format.id
where medium_format.name = 'Cassette'
order by date_year desc, date_month desc, date_day desc
limit 10;
