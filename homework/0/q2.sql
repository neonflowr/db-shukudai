select a.type, a.name, work_type.name, b.len, b.cm, 
from work a 
inner join (
    select type, MAX(comment) as cm, MAX(LEN(comment)) as len
    from work
    where LEN(comment) > 0
    group by type, name
) b on a.type = b.type
inner join work_type on a.type = work_type.id
order by a.type asc, b.cm asc;
select release.name, release_info.date_year, medium_format.name from release
inner join release_info on release_info.release = release.id
inner join artist on release.artist_credit = artist.id
inner join artist_credit on release.artist_credit = artist_credit.id
inner join medium on release.id = medium.release
inner join medium_format on medium.format = medium_format.id
where release.name like '%Please Please Me%' AND artist_credit.name like '%The Beatles%' AND release_info.date_year <= artist.end_date_year AND medium_format.name like '12" Vinyl'
group by release.name, release_info.date_year, medium_format.name
order by date_year;
