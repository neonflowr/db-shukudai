-- pso collab
-- 'Pittsburgh Symphony Orchestra'

select artist.name
from artist_credit_name
inner join artist on artist.id = artist_credit_name.artist
where artist_credit in (
select artist_credit.id
from artist_credit_name 
inner join artist_credit on artist_credit_name.artist_credit = artist_credit.id
where artist_credit_name.name = 'Pittsburgh Symphony Orchestra'
) and artist.name != 'Pittsburgh Symphony Orchestra'
group by artist.name
order by artist.name;
